package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamMicroServiceApplication {

	public static void main(String[] args) {
		// ConfigurableApplicationContext context =

		SpringApplication.run(ExamMicroServiceApplication.class, args);

	}

}
