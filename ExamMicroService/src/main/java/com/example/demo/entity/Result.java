package com.example.demo.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "result")
public class Result {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int resultId;
	private String resultStatus;
	private String totalMarks;
	private String totalQuestion;
	private String marksObtained;

	@ManyToOne(targetEntity = Quiz.class , cascade = CascadeType.ALL)
	@JoinColumn(name = "quizId")
	private Quiz quizId;

	@ManyToOne(targetEntity = User.class ,cascade = CascadeType.ALL)
	@JoinColumn(name = "userId")
	private User user;

	public Result() {
		super();
	}

	public Result(int resultId, String resultStatus, String totalMarks, String totalQuestion, String marksObtained,
			Quiz quizId, User user) {
		super();
		this.resultId = resultId;
		this.resultStatus = resultStatus;
		this.totalMarks = totalMarks;
		this.totalQuestion = totalQuestion;
		this.marksObtained = marksObtained;

		this.quizId = quizId;
		this.user = user;
	}

	public int getResultId() {
		return resultId;
	}

	public void setResultId(int resultId) {
		this.resultId = resultId;
	}

	public String getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(String resultStatus) {
		this.resultStatus = resultStatus;
	}

	public String getTotalMarks() {
		return totalMarks;
	}

	public void setTotalMarks(String totalMarks) {
		this.totalMarks = totalMarks;
	}

	public String getTotalQuestion() {
		return totalQuestion;
	}

	public void setTotalQuestion(String totalQuestion) {
		this.totalQuestion = totalQuestion;
	}

	public String getMarksObtained() {
		return marksObtained;
	}

	public void setMarksObtained(String marksObtained) {
		this.marksObtained = marksObtained;
	}

	public Quiz getQuizId() {
		return quizId;
	}

	public void setQuizId(Quiz quizId) {
		this.quizId = quizId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
