package com.example.demo.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "quizes")
public class Quiz {

	@Id
	private int quizId;

	@ManyToOne(targetEntity = Subject.class, cascade = CascadeType.ALL)
	@JoinColumn
	private Subject subject;

	private String description;

	public Quiz() {
		super();
	}

	public Quiz(int quizId, Subject subject, String description) {
		super();
		this.quizId = quizId;
		this.subject = subject;
		this.description = description;
	}

	public int getQuizId() {
		return quizId;
	}

	public void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Quiz [quizId=" + quizId + ", subject=" + subject + ", description=" + description + "]";
	}

}
