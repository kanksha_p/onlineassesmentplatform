package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.entity.Result;
import com.example.demo.exceptionHandler.NoResultFoundException;
import com.example.demo.repository.ResultRepo;

@Service
public class ResultService {

	@Autowired
	ResultRepo resultRepo;

	public List<Result> getAllResults() throws NoResultFoundException {
		List<Result> res = resultRepo.findAll();
		if (res.isEmpty()) {
			throw new NoResultFoundException("Hey Result List Is Empty");
		}
		return res;
	}

	public Result addNewResult(Result result) throws NoResultFoundException {
		if (result.getResultStatus().isEmpty() || result.getTotalMarks().isEmpty()
				|| result.getTotalQuestion().isEmpty() || result.getMarksObtained().isEmpty()) {
			throw new NoResultFoundException("Input Fields Are Empty");
		}
		return resultRepo.save(result);
	}

	public List<Result> getResultsByUserName(String userName) throws NoResultFoundException {
		List<Result> result = resultRepo.findByUserName(userName);
		if (result.isEmpty()) {
			throw new NoResultFoundException("No Result Found With UserName =" + userName);
		}
		return result;
	}

}
