package com.example.demo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.QuestionBank;
import com.example.demo.exceptionHandler.NoQuestionFoundException;
import com.example.demo.repository.QuestionBankRepo;

import jakarta.transaction.Transactional;

@Service
public class QuestionBankService {

	@Autowired
	QuestionBankRepo questionBankRepo;

	public List<QuestionBank> getAllQuestions() throws NoQuestionFoundException {
		List<QuestionBank> questions = questionBankRepo.findAll();
		if (questions.isEmpty()) {
			throw new NoQuestionFoundException("Hey Question Bank Is Empty");
		}
		return questions;
	}

	public QuestionBank addQuestion(QuestionBank question) throws NoQuestionFoundException {
		if (question.getQuestion().isEmpty() || question.getOption1().isEmpty() || question.getOption2().isEmpty()
				|| question.getOption3().isEmpty() || question.getOption4().isEmpty() || question.getAnswer().isEmpty()
				|| question.getDifficultyLevel().isEmpty()) {
			throw new NoQuestionFoundException("Input Fields Are Empty");
		}
		return questionBankRepo.save(question);
	}

	@Transactional
	public void deleteQuestionById(int id) {
		questionBankRepo.deleteById(id);
	}

	public List<QuestionBank> getQuestionById(int questionId) throws NoQuestionFoundException {
		List<QuestionBank> qns = questionBankRepo.findById(questionId);
		if (qns.isEmpty()) {
			throw new NoQuestionFoundException("No Question Found With QuestionId =" + questionId);
		}
		return qns;
	}

	public List<QuestionBank> questionsByQuizId(int quizId) throws NoQuestionFoundException {
		List<QuestionBank> qns = questionBankRepo.findByQuizId(quizId);
		if (qns.isEmpty()) {
			throw new NoQuestionFoundException("No Question Found With QuizId =" + quizId);
		}
		return qns;
	}

}
