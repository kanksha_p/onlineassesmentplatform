package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Subject;
import com.example.demo.exceptionHandler.NoSubjectFoundException;
import com.example.demo.repository.SubjectRepo;

import jakarta.transaction.Transactional;

@Service
public class SubjectService {

	@Autowired
	SubjectRepo subjectRepo;

	public List<Subject> getAllSubjects() throws NoSubjectFoundException {
		List<Subject> sub = subjectRepo.findAll();
		if (sub.isEmpty()) {
			throw new NoSubjectFoundException("Hey Subject List Is Empty");
		}
		return sub;
	}

	public Subject add(Subject subject) throws NoSubjectFoundException {
		if (subject.getSubjectName().isEmpty()) {
			throw new NoSubjectFoundException("Input Fields Are Empty");
		}
		return subjectRepo.save(subject);
	}

	@Transactional
	public void deleteSubjectBySubjectId(int subjectId) {
		subjectRepo.deleteById(subjectId);

	}

	public List<Subject> getSubjectBySubjectId(int subjectId) throws NoSubjectFoundException {
		List<Subject> subject = subjectRepo.findById(subjectId);
		if (subject.isEmpty()) {
			throw new NoSubjectFoundException("No Subject Found With SubjectId =" + subjectId);
		}
		return subject;
	}

}
