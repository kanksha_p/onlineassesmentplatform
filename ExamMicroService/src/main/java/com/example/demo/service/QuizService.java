package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.entity.Quiz;
import com.example.demo.exceptionHandler.NoQuizFoundException;
import com.example.demo.repository.QuizRepo;

import jakarta.transaction.Transactional;

@Service
public class QuizService {

	@Autowired
	QuizRepo quizRepo;

	public List<Quiz> getAllQuizes() throws NoQuizFoundException {
		List<Quiz> quiz = quizRepo.findAll();
		;
		if (quiz.isEmpty()) {
			throw new NoQuizFoundException("Hey Quiz List Is Empty");
		}
		return quiz;

	}

	public Quiz addQuiz(Quiz quiz) throws NoQuizFoundException {
		if (quiz.getDescription().isEmpty()) {
			throw new NoQuizFoundException("Input Fields Are Empty");
		}
		return quizRepo.save(quiz);
	}

	@Transactional
	public void deleteByQuizId(int id) {

		quizRepo.deleteById(id);

	}

	public List<Quiz> getQuizByQuizId(int quizId) throws NoQuizFoundException {
		List<Quiz> quiz = quizRepo.findById(quizId);
		if (quiz.isEmpty()) {
			throw new NoQuizFoundException("No Quiz Found With QuizId =" + quizId);
		}
		return quiz;
	}

}
