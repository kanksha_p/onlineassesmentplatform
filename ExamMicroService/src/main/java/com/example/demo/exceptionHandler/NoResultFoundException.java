package com.example.demo.exceptionHandler;

public class NoResultFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public NoResultFoundException() {
	}

	public NoResultFoundException(String msg) {
		super(msg);
		this.message = msg;
	}
}
