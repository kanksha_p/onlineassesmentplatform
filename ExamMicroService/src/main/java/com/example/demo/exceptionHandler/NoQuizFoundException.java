package com.example.demo.exceptionHandler;

public class NoQuizFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public NoQuizFoundException() {
	}

	public NoQuizFoundException(String message) {
		super(message);
		this.message = message;

	}
}