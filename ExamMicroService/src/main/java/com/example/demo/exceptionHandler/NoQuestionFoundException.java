package com.example.demo.exceptionHandler;

public class NoQuestionFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public NoQuestionFoundException() {
	}

	public NoQuestionFoundException(String message) {
		super(message);
		this.message = message;

	}
}
