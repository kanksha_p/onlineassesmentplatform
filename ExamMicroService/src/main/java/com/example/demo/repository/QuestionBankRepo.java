package com.example.demo.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.QuestionBank;

@Repository
public interface QuestionBankRepo extends JpaRepository<QuestionBank, Integer> {

	List<QuestionBank> findById(int questionId);

	@Query(value = "select * from question_bank where subject_subject_id = ?1", nativeQuery = true)
	List<QuestionBank> findBySubjectId(@Param("subjectId") int subjectId);

	@Query(value="select * from question_bank where quiz_quiz_id = ?1",nativeQuery =  true)
	List<QuestionBank> findByQuizId(int quizId);

}
