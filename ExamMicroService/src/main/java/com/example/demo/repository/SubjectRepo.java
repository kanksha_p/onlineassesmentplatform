package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.Subject;

@Repository
public interface SubjectRepo extends JpaRepository<Subject, Integer> {

	List<Subject> findById(int subjectId);
}
