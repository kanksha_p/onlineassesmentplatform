package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.entity.Quiz;
import com.example.demo.exceptionHandler.NoQuizFoundException;
import com.example.demo.service.QuizService;

@RestController
@CrossOrigin(origins = "*")
public class QuizController {

	@Autowired
	private QuizService quizService;

	@GetMapping("/quiz")
	public ResponseEntity<List<Quiz>> getAllQuizes() throws NoQuizFoundException {
		List<Quiz> quizes = quizService.getAllQuizes();
		return ResponseEntity.ok(quizes);
	}

	@PostMapping("/quiz")
	public ResponseEntity<Quiz> addQuiz(@RequestBody Quiz quiz) throws NoQuizFoundException {
		Quiz newQuiz = quizService.addQuiz(quiz);
		return ResponseEntity.ok(newQuiz);
	}

	@DeleteMapping("/quiz/{quizId}")
	public ResponseEntity<Void> deleteQuiz(@PathVariable("quizId") int quizId) {
		quizService.deleteByQuizId(quizId);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/quiz/{quizId}")
	public ResponseEntity<List<Quiz>> getQuizByQuizId(@PathVariable("quizId") int quizId) throws NoQuizFoundException {
		List<Quiz> quiz = quizService.getQuizByQuizId(quizId);
		return ResponseEntity.ok(quiz);
	}

}
