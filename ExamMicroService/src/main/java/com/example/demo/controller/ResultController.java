package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.entity.Result;
import com.example.demo.entity.User;
import com.example.demo.exceptionHandler.NoResultFoundException;
import com.example.demo.service.ResultService;

@RestController
@CrossOrigin(origins = "*")
public class ResultController {

	@Autowired
	private ResultService resultService;

	@GetMapping("/result")
	public ResponseEntity<List<Result>> getAllResults() throws NoResultFoundException {
		List<Result> results = resultService.getAllResults();
		return ResponseEntity.ok(results);
	}

	@PostMapping("/result")
	public ResponseEntity<Result> saveResult(@RequestBody Result result) throws NoResultFoundException {
		Result newResult = resultService.addNewResult(result);
		return ResponseEntity.ok(newResult);
	}

//	@GetMapping("/results/{userId}")
//	public ResponseEntity<List<Result>> getResults(@PathVariable int userId) {
//		List<Result> result = resultService.getResults(userId);
//		return ResponseEntity.ok(result);
//	}

	@GetMapping("/api/user/{userName}/result")
	public ResponseEntity<List<Result>> getAllResultsOfStudent(@PathVariable("username") String userName)
			throws NoResultFoundException {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getForEntity("http://localhost:8082/api/user/{userName}", User.class);
		List<Result> results = resultService.getResultsByUserName(userName);
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

}
