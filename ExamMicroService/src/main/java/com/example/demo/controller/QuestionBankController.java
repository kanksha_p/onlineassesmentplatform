package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.QuestionBank;
import com.example.demo.exceptionHandler.NoQuestionFoundException;
import com.example.demo.service.QuestionBankService;

@RestController
@CrossOrigin(origins = "*")
public class QuestionBankController {

	@Autowired
	private QuestionBankService questionBankService;

	@GetMapping("/questionBank")
	public ResponseEntity<List<QuestionBank>> getAllQuestions() throws NoQuestionFoundException {
		List<QuestionBank> quizes = questionBankService.getAllQuestions();
		return ResponseEntity.ok(quizes);
	}

	@PostMapping("/questionBank")
	public ResponseEntity<QuestionBank> addQuestion(@RequestBody QuestionBank question) throws NoQuestionFoundException {
		QuestionBank newQuestion = questionBankService.addQuestion(question);
		return ResponseEntity.ok(newQuestion);
	}

	@GetMapping("/questionBank/{questionId}")
	public ResponseEntity<List<QuestionBank>> getQuestionById(@PathVariable int questionId) throws NoQuestionFoundException {
		List<QuestionBank> question = questionBankService.getQuestionById(questionId);

		return ResponseEntity.ok(question);

	}

	@DeleteMapping("/questionBank/{questionId}")
	public ResponseEntity<Void> deleteQuestionById(@PathVariable int questionId) {
		questionBankService.deleteQuestionById(questionId);
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/questionBank/examQuestions/{quizId}")
	public ResponseEntity<List<QuestionBank>> getQuestionsByQuizId(@PathVariable int quizId) throws NoQuestionFoundException {
		List<QuestionBank> questions = questionBankService.questionsByQuizId(quizId);
		return ResponseEntity.ok(questions);
	}

//	@GetMapping("/questionBank/subject/{subjectId}")
//	public ResponseEntity<List<QuestionBank>> getQuestionsBySubjectId(@PathVariable int subjectId) {
//		List<QuestionBank> questions = questionBankService.questionsBySubjectId(subjectId);
//		return ResponseEntity.ok(questions);
//	}

//	@GetMapping("/questions/subject/{subjectId}")
//	  public ResponseEntity<QuestionBank> getQuestionsBySubject(@PathVariable int subjectId) {
//		List<QuestionBank> questions = questionBankService.getQuestionsBySubject(subjectId);
//	    QuestionBank questionEntity = new QuestionBank();
//	    questionEntity.setQuestions(questions);
//	    return ResponseEntity.ok(questionEntity);
//	  }

//	@GetMapping("/questionBank/{questionId}")
//  public ResponseEntity<QuestionBank> getQuestionsById(@PathVariable("questionId") int id) {
//		questionBankService.questionById(id);
//      return ResponseEntity.noContent().build();
//  }

//	@GetMapping("/questionBank/subject/{subjectId}")
//  public ResponseEntity<Optional<QuestionBank>> getQuestionsBySubjectId(@PathVariable("subjectId") int subjectId) {
//		Optional<QuestionBank> questions = questionBankService.questionsBySubjectId(subjectId);
//	      return ResponseEntity.ok(questions);
//  }

//	@GetMapping("/questionBank/Subject/{subjectName}")
//	public ResponseEntity<Optional<QuestionBank>> getQuestionsBySubjectName(@PathVariable String subjectName) {
//	    Optional<QuestionBank> questions = questionBankService.questionsBySubjectName(subjectName);
//	    return ResponseEntity.ok(questions);
//	}

}
