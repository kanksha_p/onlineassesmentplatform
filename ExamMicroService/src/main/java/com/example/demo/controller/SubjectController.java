package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Subject;
import com.example.demo.exceptionHandler.NoSubjectFoundException;
import com.example.demo.service.SubjectService;

@RestController
@CrossOrigin(origins = "*")
public class SubjectController {

	@Autowired
	private SubjectService subjectService;

	@GetMapping("/subject")
	public ResponseEntity<List<Subject>> getAllSubjects() throws NoSubjectFoundException {
		List<Subject> subjects = subjectService.getAllSubjects();
		return ResponseEntity.ok(subjects);
	}

	@PostMapping("/subject")
	public ResponseEntity<Subject> addSubject(@RequestBody Subject subject) throws NoSubjectFoundException {
		Subject addedSubject = subjectService.add(subject);
		return ResponseEntity.ok(addedSubject);
	}

	@DeleteMapping("/subject/{subjectId}")
	public ResponseEntity<Void> deleteSubjectBySubjectId(@PathVariable("subjectId") int subjectId) {
		subjectService.deleteSubjectBySubjectId(subjectId);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/subject/{subjectId}")
	public ResponseEntity<List<Subject>> getSubject(@PathVariable("subjectId") int subjectId) throws NoSubjectFoundException {
		List<Subject> subject = subjectService.getSubjectBySubjectId(subjectId);
		return ResponseEntity.ok(subject);
	}
}
